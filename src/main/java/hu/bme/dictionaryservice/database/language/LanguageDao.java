package hu.bme.dictionaryservice.database.language;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface LanguageDao extends CrudRepository<Language, String>{

	@Query(value="SELECT * FROM language", nativeQuery=true)
	List<Language> getAllLanguages();
	
	@Query(value="SELECT * FROM language WHERE id=?1",nativeQuery=true)
	Language getLanguage(String id);
}

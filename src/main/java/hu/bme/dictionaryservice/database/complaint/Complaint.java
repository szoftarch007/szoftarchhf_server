package hu.bme.dictionaryservice.database.complaint;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import hu.bme.dictionaryservice.database.user.User;
import hu.bme.dictionaryservice.database.word.Word;

@Entity
@Table(name = "complaint")
public class Complaint {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "username", nullable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "word_id", referencedColumnName = "text", nullable = false)
	private Word word;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "isSolved", nullable = false)
	@ColumnDefault(value = "false")
	private boolean isSolved;

	public Complaint() {
	}

	public Complaint(User user, Word word, String description, boolean isSolved) {
		this.user = user;
		this.word = word;
		this.description = description;
		this.isSolved = isSolved;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSolved() {
		return isSolved;
	}

	public void setSolved(boolean isSolved) {
		this.isSolved = isSolved;
	}

	public Word getWord() {
		return word;
	}

	public void setWord(Word word) {
		this.word = word;
	}
}

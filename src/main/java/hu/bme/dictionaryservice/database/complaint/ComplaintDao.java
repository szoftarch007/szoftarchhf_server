package hu.bme.dictionaryservice.database.complaint;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ComplaintDao extends CrudRepository<Complaint, Long> {
	
}

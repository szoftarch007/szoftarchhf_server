package hu.bme.dictionaryservice.database.picture;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface PictureDao extends CrudRepository<Picture, Long> {

}

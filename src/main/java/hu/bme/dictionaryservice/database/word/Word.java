package hu.bme.dictionaryservice.database.word;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import hu.bme.dictionaryservice.database.language.Language;

@Entity
@Table(name = "word", uniqueConstraints = {@UniqueConstraint(columnNames = "text") })
public class Word {

	@Id
	@Column(name = "text", nullable = false)
	private String text;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "link", nullable = true)
	private String link;

	@ManyToOne
	@JoinColumn(name = "language_id", referencedColumnName = "id", unique = false, nullable = false)
	private Language language;

	public Word() {
	}

	public Word(String text, String description, String link, Language language) {
		this.text = text;
		this.description = description;
		this.link = link;
		this.language = language;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Word that = (Word) o;

		if (!getText().equals(that.getText()))
			return false;

		return true;
	}

	public int hashCode() {
		return (getText().hashCode());
	}

}

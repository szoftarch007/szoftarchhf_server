package hu.bme.dictionaryservice.database.word;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@Transactional
public interface WordDao extends CrudRepository<Word, String> {

	@Query(value = "SELECT * FROM word WHERE LOWER(text) LIKE LOWER(CONCAT(:term,'%'))", nativeQuery = true)
	public List<Word> searchWords(@Param("term") String term);

	@Query(value = "SELECT * FROM word WHERE LOWER(text) LIKE LOWER(CONCAT(:term,'%')) AND language_id = :langId", nativeQuery = true)
	public List<Word> searchWords(@Param("term") String term, @Param("langId")String langId);

	@Query(value="SELECT w2.* FROM "
			+ "word w1, word w2, translation t "
			+ "WHERE (w1.text = t.to_word OR w1.text = t.from_word) AND (w2.text = t.to_word OR w2.text = t.from_word) AND (w1.text <> w2.text) AND w1.text=?1",
			nativeQuery = true)
	public List<Word> getTranslationsOf(String word);
	
	@Query(value="SELECT w2.* FROM "
			+ "word w1, word w2, translation t "
			+ "WHERE (w1.text = t.to_word OR w1.text = t.from_word) AND (w2.text = t.to_word OR w2.text = t.from_word) AND (w1.text <> w2.text) AND w1.text=?1 AND w2.language_id=?2",
			nativeQuery = true)
	public List<Word> getTranslationOf(String word, String languageId);
}

package hu.bme.dictionaryservice.database.translation;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import hu.bme.dictionaryservice.database.word.Word;

@SuppressWarnings("serial")
@Embeddable
public class TranslationId implements Serializable {

	private static final long serialVersionUID = -51547674274510964L;

	@ManyToOne
	private Word fromWord;
	
	@ManyToOne
	private Word toWord;
	
	public Word getFromWord(){
		return fromWord;
	}

	public Word getToWord() {
		return toWord;
	}

	public void setToWord(Word toWord) {
		this.toWord = toWord;
	}

	public void setFromWord(Word fromWord) {
		this.fromWord = fromWord;
	}

	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TranslationId that = (TranslationId) o;

        if (fromWord != null ? !fromWord.equals(that.fromWord) : that.fromWord != null) return false;
        if (toWord != null ? !toWord.equals(that.toWord) : that.toWord != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (fromWord != null ? fromWord.hashCode() : 0);
        result = 31 * result + (toWord != null ? toWord.hashCode() : 0);
        return result;
    }
}

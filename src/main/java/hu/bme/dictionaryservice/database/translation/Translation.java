package hu.bme.dictionaryservice.database.translation;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import hu.bme.dictionaryservice.database.picture.Picture;
import hu.bme.dictionaryservice.database.word.Word;

@Entity
@Table(name = "translation")
@AssociationOverrides({ @AssociationOverride(name = "pk_from_word", joinColumns = @JoinColumn(name = "from_word")),
		@AssociationOverride(name = "pk_to_word", joinColumns = @JoinColumn(name = "to_word")) })
public class Translation {

	@EmbeddedId
	private TranslationId id = new TranslationId();

	@ManyToOne
	@JoinColumn(name = "picture_id", referencedColumnName = "id", unique = false, nullable = true)
	private Picture picture;

	public TranslationId getId() {
		return id;
	}

	public Translation() {
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public void setId(TranslationId id) {
		this.id = id;
	}

	@Transient
	public Word getFromWord() {
		return getId().getFromWord();
	}

	@Transient
	public Word getToWord() {
		return getId().getToWord();
	}

	public void setFromWord(Word fromWord) {
		getId().setFromWord(fromWord);
	}

	public void setToWord(Word toWord) {
		getId().setToWord(toWord);
	}
}

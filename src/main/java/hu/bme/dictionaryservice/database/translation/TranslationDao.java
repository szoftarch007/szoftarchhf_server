package hu.bme.dictionaryservice.database.translation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface TranslationDao extends CrudRepository<Translation, TranslationId> {

	@Query(value = "SELECT * FROM translation WHERE from_word_text=?1 OR to_word_text=?1", nativeQuery = true)
	List<Translation> getTranslationsFor(String word);
}

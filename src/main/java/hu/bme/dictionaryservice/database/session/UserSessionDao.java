package hu.bme.dictionaryservice.database.session;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserSessionDao extends CrudRepository<UserSession, String> {
	
	@Query(value="SELECT * FROM user_session WHERE user_id=?1",nativeQuery=true)
	public UserSession getUserSession(long userId);
	
	@Query(value="SELECT * FROM user_session WHERE token=?1",nativeQuery=true)
	public UserSession getUserSession(String token);
}

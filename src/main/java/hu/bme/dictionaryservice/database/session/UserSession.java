package hu.bme.dictionaryservice.database.session;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import hu.bme.dictionaryservice.database.user.User;

@Entity
@Table(name = "user_session")
public class UserSession {

	@Id
	@Column(name = "token", nullable = false)
	private String token;

	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "username", nullable = false)
	private User user;

	public UserSession() {
	}

	public UserSession(String token, User user) {
		this.token = token;
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}

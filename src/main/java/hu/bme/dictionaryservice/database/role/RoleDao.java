package hu.bme.dictionaryservice.database.role;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface RoleDao extends CrudRepository<Role, Long>{
	
	@Query(value = "SELECT * FROM user_role WHERE id = ?1", nativeQuery = true)
	Role getRole(long id);
	
	@Query(value = "SELECT * FROM user_role WHERE name = ?1", nativeQuery = true)
	Role getRole(String name);

}

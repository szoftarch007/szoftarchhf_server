package hu.bme.dictionaryservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such method")
public class NoSuchMethodException extends RuntimeException{

}

package hu.bme.dictionaryservice.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.bme.dictionaryservice.database.user.User;

public class Authenticator {

	private Map<String, Authorization> authorizations;

	private static Authenticator instance = null;

	private Authenticator() {
		authorizations = new HashMap<>();
	}

	public static Authenticator getInstance() {
		if (instance == null) {
			instance = new Authenticator();
		}

		return instance;
	}

	public void addAuthorization(String role, String[] apis){
		authorizations.put(role, new Authorization(apis));
	}

	public String generateTokenForUser(User user) {
		String token = user.getUsername() + "%%%" + user.getRole().getName() + "%%%" + new Date();
		token = Base64.getEncoder().encodeToString(token.getBytes());
		return token;
	}
	
	public String getUsername(String token){
		String decodedToken = new String(Base64.getDecoder().decode(token.getBytes()));
		String[] splitToken = decodedToken.split("%%%");
		if (splitToken.length != 3) {
			return "";
		}

		return splitToken[0];
	}

	public boolean isAuthorized(String token, String api) {
		String role = getRoleFromToken(token);
		
		if(role.equals("admin")){
			return true;
		}
		
		if (!authorizations.containsKey(role)) {
			return false;
		}

		Authorization auth = authorizations.get(role);

		return auth.isAuthorized(api);
	}

	private String getRoleFromToken(String token) {
		String decodedToken = new String(Base64.getDecoder().decode(token.getBytes()));
		String[] splitToken = decodedToken.split("%%%");
		if (splitToken.length != 3) {
			return "";
		}

		return splitToken[1];
	}

	private class Authorization {
		private List<String> authorizedApis;

		public Authorization() {
			authorizedApis = new ArrayList<>();
		}
		
		public Authorization(String[] apis){
			authorizedApis = Arrays.asList(apis);
		}

		public void add(String api) {
			authorizedApis.add(api);
		}

		public boolean isAuthorized(String api) {
			if (authorizedApis.contains(api)) {
				return true;
			}

			return false;
		}

	}

}

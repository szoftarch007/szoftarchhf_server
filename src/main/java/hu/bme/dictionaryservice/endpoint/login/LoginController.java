package hu.bme.dictionaryservice.endpoint.login;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.bme.dictionaryservice.database.session.UserSession;
import hu.bme.dictionaryservice.database.session.UserSessionDao;
import hu.bme.dictionaryservice.database.user.User;
import hu.bme.dictionaryservice.database.user.UserDao;
import hu.bme.dictionaryservice.endpoint.EndpointConfig;
import hu.bme.dictionaryservice.model.login.Credentials;
import hu.bme.dictionaryservice.model.login.LoginResponse;

@RestController
public class LoginController {

	public static final String API_LOGIN = EndpointConfig.API_BASE + "/login";
	public static final String API_LOGOUT = EndpointConfig.API_BASE + "/logout";

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserSessionDao userSessionDao;

	private SecureRandom random = new SecureRandom();

	@RequestMapping(value = API_LOGIN, method = RequestMethod.POST)
	public ResponseEntity<LoginResponse> login(@RequestBody Credentials credentials) {

		User user = userDao.findOne(credentials.getUsername());

		if (user == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}

		if (!isPasswordValid(user.getPassword(), credentials.getPassword())) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		UserSession userSession = userSessionDao.getUserSession(user.getUsername());

		if (userSession == null) {
			userSession = new UserSession(nextSessionId(), user);
			userSessionDao.save(userSession);
		}

		return ResponseEntity.ok().body(new LoginResponse(userSession.getToken(), user.getRole()));
	}

	@RequestMapping(value = API_LOGOUT, method = RequestMethod.POST)
	public ResponseEntity<?> logout(@RequestHeader("Authorization") String token){
		
		UserSession userSession = userSessionDao.getUserSession(token);
		
		if(userSession == null){
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}
		
		userSessionDao.delete(userSession);
		
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	private boolean isPasswordValid(String storedPassword, String givenPassword) {
		return storedPassword.equals(Integer.toString(givenPassword.hashCode()));
	}

	public String nextSessionId() {
		return new BigInteger(130, random).toString(32);
	}

}

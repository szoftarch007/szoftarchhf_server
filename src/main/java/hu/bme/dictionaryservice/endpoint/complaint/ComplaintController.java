package hu.bme.dictionaryservice.endpoint.complaint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.bme.dictionaryservice.database.complaint.Complaint;
import hu.bme.dictionaryservice.database.complaint.ComplaintDao;
import hu.bme.dictionaryservice.database.user.User;
import hu.bme.dictionaryservice.database.user.UserDao;
import hu.bme.dictionaryservice.database.word.Word;
import hu.bme.dictionaryservice.database.word.WordDao;
import hu.bme.dictionaryservice.endpoint.ControllerBase;
import hu.bme.dictionaryservice.endpoint.EndpointConfig;
import hu.bme.dictionaryservice.model.complaint.ComplaintData;
import hu.bme.dictionaryservice.model.complaint.GetAllComplaintsResponse;
import hu.bme.dictionaryservice.model.complaint.GetComplaintResponse;

@RestController
public class ComplaintController extends ControllerBase {

	public static final String API_COMPLAINT = EndpointConfig.API_BASE + "/complaint";

	public static final String API_COMPLAINT_CLOSE = API_COMPLAINT + "/close";

	public static final String API_COMPLAINT_CREATE = API_COMPLAINT + "/create";

	@Autowired
	private ComplaintDao complaintDao;

	@Autowired
	private WordDao wordDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = API_COMPLAINT, method = RequestMethod.GET)
	public ResponseEntity<GetComplaintResponse> getAllComplaints(
			@RequestParam(value = "id", required = false) Long id, @RequestHeader(AUTH_HEADER) String token) {

		if (!auth.isAuthorized(token, API_COMPLAINT)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		List<Complaint> complaints = null;
		if (id == null) {
			complaints = (List<Complaint>) complaintDao.findAll();
		}else{
			complaints = (List<Complaint>) complaintDao.findOne(id);
		}

		return ResponseEntity.ok(new GetComplaintResponse(complaints));
	}

	@RequestMapping(value = API_COMPLAINT_CLOSE, method = RequestMethod.POST)
	public ResponseEntity<Void> closeComplaint(@RequestParam("id") Long id, @RequestHeader(AUTH_HEADER) String token) {

		if (!auth.isAuthorized(token, API_COMPLAINT_CLOSE)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		Complaint complaint = complaintDao.findOne(id);

		if (complaint == null) {
			return ResponseEntity.notFound().build();
		}

		complaint.setSolved(true);

		complaintDao.save(complaint);

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = API_COMPLAINT_CREATE, method = RequestMethod.POST)
	public ResponseEntity<Void> createComplaint(@RequestBody ComplaintData complaintData,
			@RequestHeader(AUTH_HEADER) String token) {

		if (!auth.isAuthorized(token, API_COMPLAINT_CREATE)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		if (complaintData == null) {
			return ResponseEntity.badRequest().build();
		}

		Word word = wordDao.findOne(complaintData.getWord());

		if (word == null) {
			return ResponseEntity.badRequest().build();
		}

		User user = userDao.findOne(auth.getUsername(token));

		Complaint complaint = new Complaint(user, word, complaintData.getDescription(), false);

		complaintDao.save(complaint);

		return ResponseEntity.ok().build();
	}

}

package hu.bme.dictionaryservice.endpoint;

import org.springframework.beans.factory.annotation.Autowired;

import hu.bme.dictionaryservice.security.Authenticator;

public abstract class ControllerBase {
	
	protected static final String AUTH_HEADER = "X-Authorization";
	
	@Autowired
	protected Authenticator auth;
	
	protected boolean isAuthorized(String token, String api){
		return auth.isAuthorized(token, api);
	}
}

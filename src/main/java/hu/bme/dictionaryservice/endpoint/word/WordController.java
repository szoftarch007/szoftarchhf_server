package hu.bme.dictionaryservice.endpoint.word;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.bme.dictionaryservice.database.picture.Picture;
import hu.bme.dictionaryservice.database.picture.PictureDao;
import hu.bme.dictionaryservice.database.translation.Translation;
import hu.bme.dictionaryservice.database.translation.TranslationDao;
import hu.bme.dictionaryservice.database.word.Word;
import hu.bme.dictionaryservice.database.word.WordDao;
import hu.bme.dictionaryservice.endpoint.ControllerBase;
import hu.bme.dictionaryservice.endpoint.EndpointConfig;
import hu.bme.dictionaryservice.model.word.TranslationRequestData;
import hu.bme.dictionaryservice.model.word.WordSearchResponse;
import hu.bme.dictionaryservice.model.word.WordTranslationResponse;

@RestController
public class WordController extends ControllerBase {

	public static final String API_WORD = EndpointConfig.API_BASE + "/word";

	public static final String API_WORD_SEARCH = API_WORD + "/search";

	public static final String API_WORD_CREATE = API_WORD + "/create";

	public static final String API_WORD_TRANSLATION_CREATE = API_WORD + "/translation/create";

	public static final String API_WORD_TRANSLATIONS = API_WORD + "/translate";

	@Autowired
	private WordDao wordDao;

	@Autowired
	private TranslationDao translationDao;

	@Autowired
	private PictureDao pictureDao;

	@RequestMapping(value = API_WORD_SEARCH, method = RequestMethod.GET)
	public ResponseEntity<WordSearchResponse> searchWord(@RequestParam("term") String term,
			@RequestParam(value = "lang", required = false) String lang) {
		List<Word> foundWords = null;
		if (lang == null) {
			foundWords = wordDao.searchWords(term);
		} else {
			foundWords = wordDao.searchWords(term, lang);
		}

		return ResponseEntity.ok().body(new WordSearchResponse(foundWords));
	}

	@RequestMapping(value = API_WORD_CREATE, method = RequestMethod.POST)
	public ResponseEntity<Void> createWord(@RequestBody Word word) {
		Word savedWord = wordDao.save(word);

		if (savedWord == null) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = API_WORD_TRANSLATION_CREATE, method = RequestMethod.POST)
	public ResponseEntity<Void> createTranslation(@RequestBody TranslationRequestData translationRequestData) {
		Translation trans = new Translation();

		Word fromWord = wordDao.findOne(translationRequestData.getFromWord());
		Word toWord = wordDao.findOne(translationRequestData.getToWord());

		if (fromWord == null || toWord == null) {
			return ResponseEntity.badRequest().build();
		}

		trans.setFromWord(fromWord);
		trans.setToWord(toWord);
		Picture pic = new Picture();
		pic.setLink(translationRequestData.getImageLink());
		Picture savedPicture = pictureDao.save(pic);

		if (savedPicture == null) {
			return ResponseEntity.badRequest().build();
		} else {
			trans.setPicture(pic);

			Translation savedTrans = translationDao.save(trans);

			if (savedTrans == null) {
				return ResponseEntity.badRequest().build();
			}
		}

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = API_WORD_TRANSLATIONS, method = RequestMethod.GET)
	public ResponseEntity<WordTranslationResponse> getTranslationsOf(@RequestParam("word") String word,
			@RequestParam(value = "lang", required = false) String lang) {
		Word sourceWord = wordDao.findOne(word);

		if (sourceWord == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}

		List<Word> foundWords = new ArrayList<Word>();

		List<Translation> translations = translationDao.getTranslationsFor(word);

		WordTranslationResponse response = new WordTranslationResponse();

		String imageUrl = null;

		for (Translation tr : translations) {
			Word w1 = tr.getFromWord();
			Word w2 = tr.getToWord();

			Word foundWord = null;

			if (!w1.equals(sourceWord)) {
				foundWord = w1;
			}
			if (!w2.equals(sourceWord)) {
				foundWord = w2;
			}

			if (imageUrl == null && tr.getPicture().getLink() != null) {
				imageUrl = tr.getPicture().getLink();
			}

			if (lang != null) {
				if (foundWord.getLanguage().getId().equals(lang)) {
					foundWords.add(foundWord);
				}
			} else {
				foundWords.add(foundWord);
			}
		}

		response.setImageUrl(imageUrl);
		response.setSourceWord(sourceWord);
		response.setTranslations(foundWords);

		return ResponseEntity.ok(response);
	}

}

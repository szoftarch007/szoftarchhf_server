package hu.bme.dictionaryservice.endpoint;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import hu.bme.dictionaryservice.security.Authenticator;

@Configuration
public class EndpointConfig {
	public static final String API_BASE = "/dictionary";
	
	@Bean
	public Authenticator getAuthenticator(){
		return Authenticator.getInstance();
	}
}

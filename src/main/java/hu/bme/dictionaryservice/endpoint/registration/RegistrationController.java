package hu.bme.dictionaryservice.endpoint.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.bme.dictionaryservice.database.role.RoleDao;
import hu.bme.dictionaryservice.database.user.User;
import hu.bme.dictionaryservice.database.user.UserDao;
import hu.bme.dictionaryservice.endpoint.EndpointConfig;
import hu.bme.dictionaryservice.model.login.Credentials;
import hu.bme.dictionaryservice.model.registration.RegistrationResponse;

@RestController
public class RegistrationController {
	
	public static final String API_REGISTER = EndpointConfig.API_BASE + "/register";
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;

	@RequestMapping(value = API_REGISTER, method = RequestMethod.POST)
	public ResponseEntity<RegistrationResponse> register(@RequestBody Credentials credentials) {
		
		if(userDao.findOne(credentials.getUsername()) == null){
			User user = new User();
			user.setUsername(credentials.getUsername());
			user.setPassword(Integer.toString(credentials.getPassword().hashCode()));
			user.setRole(roleDao.getRole("user"));
			
			userDao.save(user);
			
			return ResponseEntity.status(HttpStatus.CREATED).body(null);
		}else{
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}
	}
}

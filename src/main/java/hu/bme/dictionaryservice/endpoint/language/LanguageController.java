package hu.bme.dictionaryservice.endpoint.language;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.bme.dictionaryservice.database.language.Language;
import hu.bme.dictionaryservice.database.language.LanguageDao;
import hu.bme.dictionaryservice.endpoint.ControllerBase;
import hu.bme.dictionaryservice.endpoint.EndpointConfig;
import hu.bme.dictionaryservice.model.language.AvailableLanguagesResponse;
import hu.bme.dictionaryservice.model.language.LanguageResponse;


@RestController
public class LanguageController extends ControllerBase{
	public static final String API_LANGUAGE = EndpointConfig.API_BASE + "/language";

	public static final String API_LANGUAGE_ALL = API_LANGUAGE + "/all";
	
	public static final String API_LANGUAGE_GET = API_LANGUAGE + "/get";
	
	@Autowired
	private LanguageDao languageDao;
	
	@RequestMapping(value=API_LANGUAGE_ALL, method=RequestMethod.GET)
	public ResponseEntity<AvailableLanguagesResponse> getAvailableLanguages(){
		
		List<Language> allLanguages = languageDao.getAllLanguages();
		
		return ResponseEntity.ok(new AvailableLanguagesResponse(allLanguages));
	}
	
	@RequestMapping(value=API_LANGUAGE_GET, method=RequestMethod.GET)
	public ResponseEntity<LanguageResponse> getLanguage(@RequestParam("code") String id){
		
		Language language = languageDao.getLanguage(id);
		
		if(language == null){
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		
		return ResponseEntity.ok(new LanguageResponse(language));
	}
}

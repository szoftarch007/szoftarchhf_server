package hu.bme.dictionaryservice.model;

public class Error {
	private String error;

	public Error() {

	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}

package hu.bme.dictionaryservice.model.complaint;

import java.util.List;

import hu.bme.dictionaryservice.database.complaint.Complaint;

public class GetAllComplaintsResponse {

	private List<Complaint> complaints;

	public GetAllComplaintsResponse() {
	}

	public GetAllComplaintsResponse(List<Complaint> complaints) {
		this.complaints = complaints;
	}

	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}
	
	

}

package hu.bme.dictionaryservice.model.complaint;

public class ComplaintData {
	private String word;
	private String description;

	public ComplaintData(String word, String description) {
		this.word = word;
		this.description = description;
	}

	public ComplaintData() {
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

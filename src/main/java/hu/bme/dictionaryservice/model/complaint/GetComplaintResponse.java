package hu.bme.dictionaryservice.model.complaint;

import java.util.List;

import hu.bme.dictionaryservice.database.complaint.Complaint;

public class GetComplaintResponse {

	private List<Complaint> complaints;

	public GetComplaintResponse(List<Complaint> complaints) {
		this.complaints = complaints;
	}

	public GetComplaintResponse() {
	}

	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}

}

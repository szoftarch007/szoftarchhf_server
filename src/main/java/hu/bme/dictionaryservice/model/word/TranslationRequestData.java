package hu.bme.dictionaryservice.model.word;

public class TranslationRequestData {
	private String fromWord;
	private String toWord;
	private String imageLink;

	public TranslationRequestData() {
	}

	public TranslationRequestData(String fromWord, String toWord, String imageLink) {
		this.fromWord = fromWord;
		this.toWord = toWord;
		this.imageLink = imageLink;
	}

	public String getFromWord() {
		return fromWord;
	}

	public void setFromWord(String fromWord) {
		this.fromWord = fromWord;
	}

	public String getToWord() {
		return toWord;
	}

	public void setToWord(String toWord) {
		this.toWord = toWord;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

}

package hu.bme.dictionaryservice.model.word;

import java.util.List;

import hu.bme.dictionaryservice.database.word.Word;

public class WordSearchResponse {

	private List<Word> foundWords;

	public WordSearchResponse(List<Word> foundWords) {
		this.foundWords = foundWords;
	}

	public WordSearchResponse() {
	}

	public List<Word> getFoundWords() {
		return foundWords;
	}

	public void setFoundWords(List<Word> foundWords) {
		this.foundWords = foundWords;
	}
	
}

package hu.bme.dictionaryservice.model.word;

import java.util.List;

import hu.bme.dictionaryservice.database.word.Word;

public class WordTranslationResponse {
	private Word sourceWord;
	private List<Word> translations;
	private String imageUrl;

	public WordTranslationResponse() {
	}

	public Word getSourceWord() {
		return sourceWord;
	}

	public void setSourceWord(Word sourceWord) {
		this.sourceWord = sourceWord;
	}

	public List<Word> getTranslations() {
		return translations;
	}

	public void setTranslations(List<Word> translations) {
		this.translations = translations;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}

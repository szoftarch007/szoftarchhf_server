package hu.bme.dictionaryservice.model.login;

import hu.bme.dictionaryservice.database.role.Role;

public class LoginResponse {
	private String sessionToken;
	private Role role;

	public LoginResponse(String sessionToken, Role role) {
		this.sessionToken = sessionToken;
		this.role = role;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}

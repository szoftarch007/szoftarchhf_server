package hu.bme.dictionaryservice.model.language;

import hu.bme.dictionaryservice.database.language.Language;

public class LanguageResponse {
	private Language language;

	public LanguageResponse() {
	}

	public LanguageResponse(Language language) {
		this.language = language;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}
	
}

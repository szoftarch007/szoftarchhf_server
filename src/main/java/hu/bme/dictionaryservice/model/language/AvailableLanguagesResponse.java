package hu.bme.dictionaryservice.model.language;

import java.util.List;

import hu.bme.dictionaryservice.database.language.Language;

public class AvailableLanguagesResponse {
	private List<Language> languages;

	public AvailableLanguagesResponse(List<Language> languages) {
		this.languages = languages;
	}

	public AvailableLanguagesResponse() {
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}
	
	
}

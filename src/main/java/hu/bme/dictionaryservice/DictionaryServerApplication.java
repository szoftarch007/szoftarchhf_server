package hu.bme.dictionaryservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import hu.bme.dictionaryservice.endpoint.complaint.ComplaintController;
import hu.bme.dictionaryservice.endpoint.language.LanguageController;
import hu.bme.dictionaryservice.endpoint.login.LoginController;
import hu.bme.dictionaryservice.endpoint.word.WordController;
import hu.bme.dictionaryservice.security.Authenticator;

@SpringBootApplication
public class DictionaryServerApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DictionaryServerApplication.class);
	}

	public static void main(String[] args) {
		
		String[] userPermittedApis = {
				ComplaintController.API_COMPLAINT_CREATE,
				LanguageController.API_LANGUAGE_ALL,
				LanguageController.API_LANGUAGE_GET,
				WordController.API_WORD_SEARCH,
				WordController.API_WORD_TRANSLATIONS
		};
		
		Authenticator.getInstance().addAuthorization("user", userPermittedApis);
		
		SpringApplication.run(DictionaryServerApplication.class, args);
	}
}
